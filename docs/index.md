# Bienvenue sur le site d'OpenBudget

* Ce site est encore en construction, et évolue au fil des contributions.
* OpenBudget.fr utilise et déploie en France les solutions Open-Source développées dans le cadre du projet [OpenBudgets.eu](http://openbudgets.eu/)
* OpenBudget.fr est rataché à la communauté [F0rk](https://f0rk.fr/) pour la création et de développement de biens communs numériques.

