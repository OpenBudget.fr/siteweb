# Questionnaire

Quels sont pour vous les budgets prioritaires à travailler pour en faciliter l'accès ? 
Quelles sont les questions que vous souhaiteriez voir adressées ? 

N'hésitez pas à remplir le formulaire ci-dessous pour nous faire connaitre votre avis et orienter notre action.

<div class="typeform-widget" data-url="https://openbudget.typeform.com/to/Sl83fK" style="width: 100%; height: 500px;">
	
</div>
<script>(function(){var qs,js,q,s,d=document,gi=d.getElementById,ce=d.createElement,gt=d.getElementsByTagName,id="typef_orm",b="https://s3-eu-west-1.amazonaws.com/share.typeform.com/";if(!gi.call(d,id)){js=ce.call(d,"script");js.id=id;js.src=b+"widget.js";q=gt.call(d,"script")[0];q.parentNode.insertBefore(js,q)}})()</script>

