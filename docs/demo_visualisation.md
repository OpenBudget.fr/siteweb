# Exemples de data-viz

Nota : le premier chargement des visualisations peut être un peu long.

## Répartition des dépenses suivant une classification économique 

<iframe src="https://tinyurl.com/y7gkfouf" width="960" height="450" border="0" frameborder="0" seamless="on" style="border: 0px; margin: 0px; padding: 0px;"></iframe>


## Dépenses liées à l'eau et l'assainissement par fonction sur les années 2013,2014,2015

<iframe src="https://tinyurl.com/yd7tb26r" width="960" height="450" border="0" frameborder="0" seamless="on" style="border: 0px; margin: 0px; padding: 0px;"></iframe>

