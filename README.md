# utilisation rapide 

* Pour éditer le contenu du site il faut éditer les fichiers et dossiers présents dans le dossiers "docs"
* La syntaxe à utiliser pour éditer les textes est le Markdown : 
 * [Documentation pour l'utilisation du markdown](http://commonmark.org/help/)
* Lors de la sauvegarde des modifications sur l'interface de Framagit, la génération du site est visualisable sur cette [page](https://framagit.org/OpenBudget.fr/siteweb/pipelines)
* Lorsque la génération est réalisée, les modifications apportées sont disponible sur le site du projet 
 * [Voir le site du projet sur framagit](https://openbudget.fr.frama.io/siteweb/)


# explication du fonctionnement : 

* La création et la publication de ce site reposent sur les principes de génération de sites statiques et ceux d'intégration/déploiement continue basés sur les repository de type Git.

* Les choix effectués dans le cadre de cette première version du site web sont les suivants : 
 * génération de site statiques : mkdocs
 * CI/CD tools & Git : framagit.


* Pour suivre l'état du processus de construction du site web, rendez-vous sur cette page : 
 * https://framagit.org/OpenBudget.fr/siteweb/pipelines

* pour modifier les contenus du site web, utiliser directement l'interface d'édition de framagit.
* Les modifications faites sur la branche master sont automatiquement publiées.
* https://framagit.org/OpenBudget.fr/siteweb/tree/master/docs


* Le site web est pour l'instant disponible à cette adresse : https://openbudget.fr.frama.io/siteweb/